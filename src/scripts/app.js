document.addEventListener("DOMContentLoaded", function() {
    var openButton = document.querySelector(".btn-open-menu");
    var imgBox = document.querySelectorAll(".gallery-image");

    function loadImgs() {
        var imgContent = ["assets/img/github.png", "assets/img/codepen.png", "assets/img/library.png", "assets/img/machepsd.png"];
        var bigPics = document.querySelectorAll(".gallery-image__picture");
        for (var i = 0, len = bigPics.length; i < len; i++) {
            bigPics[i].src = imgContent[i]
        }
    };

    function toggleNavbar() {
        var navigation = document.querySelector(".nav");
        if (navigation.className === "nav") {
            navigation.classList.add("open");
        } else {
            navigation.classList.remove("open");
        }
    };

    function toggleImg() {
        var source = this.children[0];
        var img = this.children[1];

        if (this.className === "gallery-image") {
            this.classList.add("showImg");
            img.classList.add("resizeImg");
            source.classList.add("dn");
        } else {
            this.classList.remove("showImg");
            img.classList.remove("resizeImg");
            source.classList.remove("dn");
        }
    }

    imgBox.forEach(function(item) {
        item.addEventListener("click", toggleImg);
    });

    // Old version of forEach, on top I made better one
    // for (var i = 0, length = imgBox.length; i < length; i++) {
    //   imgBox[i].addEventListener("click", toggleImg);
    // }
    openButton.addEventListener("click", toggleNavbar);
    loadImgs();
});