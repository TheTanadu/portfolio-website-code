document.addEventListener("DOMContentLoaded", function() {
  function navBar() {
    document.querySelector(".nav__toggle").addEventListener("click", function() {
      let elem = document.querySelector(".nav-links");
      elem.classList.toggle("nav-links--visible");
    })
  }
  function goBefore(num, max) {
    num = num - 1;
    if (num < 0) {
      return max;
    } else {
      return num;
    }
  }
  function goNext(num, max) {
    num = num + 1;
    if (num > max) {
      return 0;
    } else {
      return num;
    }
  }
  function slider() {
  let quotes = [
    "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.",
    "Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.",
    "Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem."
  ];
  let before = document.querySelector(".testimonials__left-arrow");
  let next = document.querySelector(".testimonials__right-arrow");
  let box = document.querySelector(".testimonial__text");
  let count = 0;
  let max = quotes.length - 1;
  box.innerHTML = quotes[count];
  before.addEventListener("click", function() {
    count = goBefore(count, max);
    box.innerHTML = quotes[count];
  });
  next.addEventListener("click", function() {
    count = goNext(count, max);
    box.innerHTML = quotes[count];
  });
}

  navBar();
  slider();
});
